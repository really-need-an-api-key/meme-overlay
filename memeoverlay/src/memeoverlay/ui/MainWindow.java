package memeoverlay.ui;

import memeoverlay.MemeOverlay;
import memeoverlay.MemeOverlay.MemeOverlayOptions;
import net.miginfocom.swing.MigLayout;
import com.xenoage.util.gui.FontChooserComboBox;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetAdapter;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URI;

public class MainWindow extends JFrame
{
    // should've declared them in constr and used closures in lambdas tbh
    private String last_txt_int = "", last_txt_file = "", last_txt_direct = "";
    private final JRadioButton radio_txt_int, radio_txt_file, radio_txt_direct;
    private final PlaceholderTextField tfield_input;
    private final JTextField tfield_int_filter;
    private final JSpinner spin_font_scale_min;
    private final JSpinner spin_font_scale_max;
    private final JLabel image_drop;
    private final JRadioButton radio_bg_transparent;
    private final JRadioButton radio_bg_coloured;
    private final ColourPanel radio_bg_colour;
    private final FontChooserComboBox combo_font;
    private final JSpinner spin_font_size;
    private final JToggleButton togg_font_bold;
    private final JToggleButton togg_font_ital;
    private final JCheckBox check_font_mod;
    private final JButton button_ok;
    private final JSpinner spin_img_width;
    private final JSpinner spin_img_height;
    private final JSpinner spin_img_cols;

    private final JSpinner spin_img_rows;
    private BufferedImage input_image;

    private MainWindow()
    {
        super("lame overlay v161114");
        // TODO: Fix font dropdown blowing up in size when font contains a long name.
        // setResizable(false);
        setLayout(new MigLayout());
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JProgressBar progress_bar = new JProgressBar();
        getContentPane().add(progress_bar, "dock south");

        button_ok = new JButton(new AbstractAction("Make it!") {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                new Thread(() -> {
                    try {
                        button_ok.setEnabled(false);
                        MemeOverlayOptions m = new MemeOverlayOptions();
                        m.image = input_image;
                        m.bg_transparent = radio_bg_transparent.isSelected();
                        m.bg_colour = radio_bg_colour.col;
                        m.bg_colour = radio_bg_colour.col;
                        m.font_name = combo_font.getSelectedFontName();
                        m.font_style = Font.PLAIN + (togg_font_bold.isSelected() ? Font.BOLD : 0) + (togg_font_ital.isSelected() ? Font.ITALIC : 0);
                        m.font_size = (int) spin_font_size.getValue();
                        m.font_size_mod = check_font_mod.isSelected();
                        m.font_size_min = (double) spin_font_scale_min.getValue();
                        m.font_size_max = (double) spin_font_scale_max.getValue();
                        m.out_w = (int) spin_img_width.getValue();
                        m.out_h = (int) spin_img_height.getValue();
                        m.cols = (int) spin_img_cols.getValue();
                        m.rows = (int) spin_img_rows.getValue();

                        SwingUtilities.invokeLater(() -> {
                            progress_bar.setStringPainted(true);
                            progress_bar.setString("Preparing text");
                        });

                        m.text = radio_txt_direct.isSelected() ? tfield_input.getText() : radio_txt_file.isSelected() ? MemeOverlay.text_from_file(tfield_input.getText()) : MemeOverlay.text_from_int(tfield_input.getText(), tfield_int_filter.getText());

                        SwingUtilities.invokeLater(() -> {
                            progress_bar.setValue(30);
                            progress_bar.setString("Creating image");
                        });

                        long timer_start = System.nanoTime();
                        BufferedImage result = MemeOverlay.make_image(m);
                        long timer_end = System.nanoTime() - timer_start;
                        System.out.println("Elapsed: " + timer_end + "ns");

                        SwingUtilities.invokeLater(() -> {
                            progress_bar.setValue(80);
                            progress_bar.setString("Saving image");
                        });

                        JFileChooser jfc = new JFileChooser(new File(image_drop.getToolTipText()).getParent());
                        if (jfc.showSaveDialog(MainWindow.this) == JFileChooser.APPROVE_OPTION)
                        {
                            File selected_file = jfc.getSelectedFile();
                            if (!selected_file.getName().endsWith(".png")) selected_file = new File(selected_file.getAbsolutePath() + ".png");
                            ImageIO.write(result, "png", selected_file);
                        }
                    }
                    catch (IOException ex) {throw new RuntimeException("There was an error saving image", ex);}
                    catch (MemeOverlay.TextProcessingException ex) {throw new RuntimeException("There was an error processing text", ex);}
                    catch (MemeOverlay.ImageProcessingException ex) {throw new RuntimeException("There was an error creating image", ex);}
                    finally
                    {
                        SwingUtilities.invokeLater(() -> {
                            progress_bar.setValue(100);
                            progress_bar.setStringPainted(false);
                            button_ok.setEnabled(true);
                        });
                    }
                }).start();
            }
        });
        getContentPane().add(button_ok, "dock south, gaptop related");

        image_drop = new JLabel("Drop image here", JLabel.CENTER);
        image_drop.setBorder(BorderFactory.createDashedBorder(null, 4, 1));
        image_drop.setMinimumSize(new Dimension(450, 450));
        image_drop.setDropTarget(new DropTarget(image_drop, new DropTargetAdapter() {
            @Override
            public void drop(DropTargetDropEvent dtde) {
                dtde.acceptDrop(DnDConstants.ACTION_COPY_OR_MOVE);
                Transferable t = dtde.getTransferable();
                try
                {
                    @SuppressWarnings("unchecked")
                    File f = ((java.util.List<File>) t.getTransferData(DataFlavor.javaFileListFlavor)).get(0);
                    if (f.isFile())
                    {
                        input_image = ImageIO.read(f);
                        BufferedImage new_img = new BufferedImage(image_drop.getWidth(), image_drop.getHeight(), BufferedImage.TYPE_INT_RGB);
                        Graphics2D g2d = new_img.createGraphics();
                        g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
                        g2d.drawImage(input_image, 0, 0, image_drop.getWidth(), image_drop.getHeight(), null);
                        g2d.dispose();
                        image_drop.setIcon(new ImageIcon(new_img));
                        image_drop.setToolTipText(f.getAbsolutePath());
                        image_drop.setText(null);

                        spin_img_width.setValue(10 * input_image.getWidth());
                        spin_img_height.setValue(-1);
                        spin_img_cols.setValue(Math.max(input_image.getWidth() / 8, 1));
                        spin_img_rows.setValue(Math.max(input_image.getHeight() / 8, 1));

                        spin_font_size.setValue(10 * input_image.getWidth() / (int) (spin_img_cols.getValue()));
                    }
                }
                catch (RuntimeException e) {throw e;}
                catch (Exception e) {throw new RuntimeException();}
            }
        }));
        getContentPane().add(image_drop, "dock west, gapright related");

        getContentPane().add(new JLabel("Text options"), "span, wrap");

        radio_txt_int = new JRadioButton(new AbstractAction("INT dir") {
            @Override
            public void actionPerformed(ActionEvent e) {
                tfield_input.setText(last_txt_int);
                tfield_int_filter.setEnabled(true);
            }
        });
        radio_txt_file = new JRadioButton(new AbstractAction("file") {
            @Override
            public void actionPerformed(ActionEvent e) {
                tfield_input.setText(last_txt_file);
                tfield_int_filter.setEnabled(false);
            }
        });
        radio_txt_direct = new JRadioButton(new AbstractAction("type directly") {
            @Override
            public void actionPerformed(ActionEvent e) {
                tfield_input.setText(last_txt_direct);
                tfield_int_filter.setEnabled(false);
            }
        });
        ButtonGroup group_txt = new ButtonGroup();
        group_txt.add(radio_txt_int);
        group_txt.add(radio_txt_file);
        group_txt.add(radio_txt_direct);
        group_txt.setSelected(radio_txt_direct.getModel(), true);
        getContentPane().add(radio_txt_int, "span 2, split 3");
        getContentPane().add(radio_txt_file);
        getContentPane().add(radio_txt_direct, "wrap");

        tfield_input = new PlaceholderTextField("type or drop folder / file");
        tfield_input.setDropTarget(new DropTarget(tfield_input, new DropTargetAdapter() {

            @Override
            public void drop(DropTargetDropEvent dtde) {
                dtde.acceptDrop(DnDConstants.ACTION_COPY_OR_MOVE);
                Transferable t = dtde.getTransferable();
                File file = null;
                try
                {
                    file = ((java.util.List<File>)t.getTransferData(DataFlavor.javaFileListFlavor)).get(0);
                }
                catch (UnsupportedFlavorException e) {throw new RuntimeException("Unable to get file from drop", e);}
                catch (IOException e) {throw new RuntimeException("File read error", e);}

                preserve_strings();

                if (file.isDirectory())
                {
                    radio_txt_int.setSelected(true);
                    tfield_int_filter.setEnabled(true);
                }
                else if (file.isFile())
                {
                    radio_txt_file.setSelected(true);
                    tfield_int_filter.setEnabled(false);
                }

                tfield_input.setText(file.getAbsolutePath());
            }
        }));
        tfield_input.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                preserve_strings();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                preserve_strings();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                preserve_strings();
            }
        });
        getContentPane().add(tfield_input, "spanx, growx, wrap");

        tfield_int_filter = new JTextField(".*");
        tfield_int_filter.setEnabled(false);
        getContentPane().add(new JLabel("Dialogue filter"));
        getContentPane().add(tfield_int_filter, "growx, wrap");

        getContentPane().add(new JLabel("Image options"), "span, wrap");

        radio_bg_transparent = new JRadioButton("transparent");
        radio_bg_coloured = new JRadioButton("coloured");
        radio_bg_colour = new ColourPanel();
        radio_bg_colour.setMinimumSize(new Dimension(20, 20));
        radio_bg_colour.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                Color selected = JColorChooser.showDialog(MainWindow.this, "Choose colour", radio_bg_colour.col);
                if (selected != null)
                {
                    radio_bg_coloured.setSelected(true);
                    radio_bg_colour.col = selected;
                    radio_bg_colour.repaint();
                }
            }
        });
        ButtonGroup group_bg = new ButtonGroup();
        group_bg.add(radio_bg_transparent);
        group_bg.add(radio_bg_coloured);
        group_bg.setSelected(radio_bg_coloured.getModel(), true);
        getContentPane().add(new JLabel("Background"));
        getContentPane().add(radio_bg_transparent, "span 2, split 3");
        getContentPane().add(radio_bg_coloured);
        getContentPane().add(radio_bg_colour, "wrap");

        spin_img_width = new JSpinner(new SpinnerNumberModel(0, -1, 20000, 1));
        spin_img_height = new JSpinner(new SpinnerNumberModel(0, -1, 20000, 1));
        getContentPane().add(new JLabel("Dimensions"));
        getContentPane().add(spin_img_width, "growx, split 3");
        getContentPane().add(new JLabel("x"));
        getContentPane().add(spin_img_height, "growx, wrap");

        spin_img_cols = new JSpinner(new SpinnerNumberModel(1, 1, 1000, 1));
        spin_img_rows = new JSpinner(new SpinnerNumberModel(1, 1, 1000, 1));
        getContentPane().add(new JLabel("Cols/rows"));
        getContentPane().add(spin_img_cols, "growx, split 3");
        getContentPane().add(new JLabel("x"));
        getContentPane().add(spin_img_rows, "growx, wrap");

        combo_font = new FontChooserComboBox();
        combo_font.setSelectedItem("Arial"); // some sane default
        combo_font.setMaximumSize(new Dimension(300, 30));
        getContentPane().add(new JLabel("Font"));
        getContentPane().add(combo_font, "growx, wrap");

        spin_font_size = new JSpinner(new SpinnerNumberModel(10, 1, 128, 1));
        togg_font_bold = new JToggleButton("B");
        togg_font_bold.setFont(togg_font_bold.getFont().deriveFont(Font.BOLD));
        togg_font_bold.setMinimumSize(new Dimension(30, 30));
        togg_font_bold.setToolTipText("Bold");
        togg_font_ital = new JToggleButton("I");
        togg_font_ital.setFont(togg_font_ital.getFont().deriveFont(Font.ITALIC));
        togg_font_ital.setMinimumSize(new Dimension(30, 30));
        togg_font_ital.setToolTipText("Italic");
        getContentPane().add(new JLabel("Font size"));
        getContentPane().add(spin_font_size, "growx, split 3");
        getContentPane().add(togg_font_bold);
        getContentPane().add(togg_font_ital, "wrap");

        check_font_mod = new JCheckBox("Font size mod");
        JComboBox<String> combo_font_scale = new JComboBox<>(new String[]{"Brightness"});
        spin_font_scale_min = new JSpinner(new SpinnerNumberModel(0.5, -10, 10, 0.1));
        spin_font_scale_max = new JSpinner(new SpinnerNumberModel(2.0, -10, 10, 0.1));
        getContentPane().add(check_font_mod);
        getContentPane().add(combo_font_scale, "growx, wrap");
        getContentPane().add(spin_font_scale_min, "growx, span 2, split 3");
        getContentPane().add(new JLabel("-"));
        getContentPane().add(spin_font_scale_max, "growx, wrap");

        JLabel website_link = new JLabel("<html><a href>Open website</a></html>", SwingConstants.RIGHT);
        website_link.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        website_link.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                try
                {
                    Desktop.getDesktop().browse(new URI("https://gitlab.com/really-need-an-api-key/meme-overlay"));
                }
                catch (Exception ex) {throw new RuntimeException(ex);}
            }
        });
        getContentPane().add(website_link, "alignx right, aligny bottom, gaptop unrelated, span 2");

        pack();
        setLocationRelativeTo(null);
    }

    // this could probably be done with a custom Document in the textfield, but whatever
    private void preserve_strings()
    {
        // Java in charge of returning JRadioButton from ButtonGroup easily
        if (radio_txt_int.isSelected())
            last_txt_int = tfield_input.getText();
        else if (radio_txt_file.isSelected())
            last_txt_file = tfield_input.getText();
        else if (radio_txt_direct.isSelected())
            last_txt_direct = tfield_input.getText();
    }

    public static void main(String[] args)
    {
        try
        {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            JFrame.setDefaultLookAndFeelDecorated(true);
            JDialog.setDefaultLookAndFeelDecorated(true);
        }
        catch (Exception ex) {ex.printStackTrace(System.err);}

        SwingUtilities.invokeLater(() -> {
            final MainWindow mw = new MainWindow();
            Thread.setDefaultUncaughtExceptionHandler((thread, throwable) -> {
                throwable.printStackTrace(System.err);

                StringBuilder message = new StringBuilder(throwable.getMessage());
                message.append(":").append(System.lineSeparator());
                Throwable cause = throwable.getCause();
                if (cause != null && cause.getMessage() != null)
                    message.append(cause.getMessage());
                else
                    message.append("Unknown error :(");

                JOptionPane.showMessageDialog(mw, message.toString(), "Error", JOptionPane.ERROR_MESSAGE);
            });
            mw.setVisible(true);
        });
    }

    private class ColourPanel extends JPanel
    {
        Color col = Color.black;

         ColourPanel()
         {
             super();
             setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
         }

        @Override
        public void paintComponent(Graphics g)
        {
            super.paintComponent(g);
            g.setColor(col);
            g.fillRect(0, 0, getWidth(), getHeight());
        }
    }

    private class PlaceholderTextField extends JTextField
    {
        String placeholder_text;

        PlaceholderTextField(String text)
        {
            super();
            placeholder_text = text;
        }

        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);

            if (getText().length() == 0)
            {
                Graphics2D g2d = (Graphics2D) g;
                FontMetrics fm = g2d.getFontMetrics();
                Color label_color = UIManager.getColor("Label.foreground");
                g2d.setPaint(new Color(0x3CFFFFFF & label_color.getRGB(), true));
                g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
                g2d.drawString(placeholder_text, 8, (getHeight() + fm.getAscent() - fm.getDescent())/2);
                g2d.dispose();
            }
        }
    }
}
