package memeoverlay;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class MemeOverlay
{
    public static class MemeOverlayOptions
    {
        public BufferedImage image;
        public String font_name = "Arial";
        public int font_style = Font.PLAIN;
        public double font_size = 10;
        public boolean font_size_mod = false;
        public double font_size_min = 0.5;
        public double font_size_max = 2.0;
        public int out_w, out_h;
        public int cols, rows;
        public String text;
        public boolean bg_transparent = false;
        public Color bg_colour = Color.black;

        public boolean debug_draw = false;
    }

    public static class TextProcessingException extends Exception
    {
        public TextProcessingException() {super();}
        public TextProcessingException(String message) {super(message);}
        public TextProcessingException(Throwable cause) {super(cause);}
        public TextProcessingException(String message, Throwable cause) {super(message, cause);}
    }

    public static class ImageProcessingException extends Exception
    {
        public ImageProcessingException() {super();}
        public ImageProcessingException(String message) {super(message);}
        public ImageProcessingException(Throwable cause) {super(cause);}
        public ImageProcessingException(String message, Throwable cause) {super(message, cause);}
    }

    public static BufferedImage make_image(MemeOverlayOptions options) throws ImageProcessingException
    {
        if (options.image == null) throw new ImageProcessingException("Image not loaded");
        if (options.text == null || options.text.length() == 0) throw new ImageProcessingException("Cannot create image with no text");

        int orig_w;
        int orig_h;

        System.out.println("Creating colour resample");
        BufferedImage scaled_image = new BufferedImage(options.cols, options.rows, BufferedImage.TYPE_INT_ARGB);
        {
            orig_w = options.image.getWidth();
            orig_h = options.image.getHeight();
            Graphics2D g2d = (Graphics2D) scaled_image.getGraphics();
            g2d.drawImage(options.image.getScaledInstance(options.cols, options.rows, Image.SCALE_AREA_AVERAGING), 0, 0, null);
            g2d.dispose();
        }

        System.out.println("Calculating dimensions");
        if (options.out_w == -1 && options.out_h == -1) throw new ImageProcessingException("Cannot infer both dimensions");

        if (options.out_w == -1)
            options.out_w = (int) ((options.out_h / (double) orig_h) * orig_w);
        else if (options.out_h == -1)
            options.out_h = (int) ((options.out_w / (double) orig_w) * orig_h);

        System.out.printf("Dimensions: %dx%d%n", options.out_w, options.out_h);
        if (options.out_w == 0 || options.out_h == 0) throw new ImageProcessingException("Hope you have a big magnifying glass");

        double fontbox_x = options.out_w / (double) options.cols;
        double fontbox_y = options.out_h / (double) options.rows;
        System.out.println("Fontbox: " + fontbox_x + "x" + fontbox_y);

        System.out.println("Creating output image");
        BufferedImage result = new BufferedImage((int) (options.out_w + options.font_size), (int) (options.out_h + options.font_size), BufferedImage.TYPE_INT_ARGB);

        Graphics2D g2d = (Graphics2D) result.getGraphics();
        g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

        // clear the background (or not)
        if (!options.bg_transparent)
        {
            g2d.setBackground(options.bg_colour);
            g2d.clearRect(0, 0, result.getWidth(), result.getHeight());
        }

        g2d.setFont(new Font(options.font_name, options.font_style, (int) options.font_size));
        g2d.translate(options.font_size / 2, options.font_size / 2);
        FontMetrics fm = g2d.getFontMetrics();
        double y_char = (fm.getAscent() - fm.getDescent()) / 2.0;

        System.out.println("Drawing text");
        int char_idx = 0;
        for (int y = 0; y < options.rows; ++y)
        {
            for (int x = 0; x < options.cols; ++x)
            {
                double out_x_point = options.out_w * x / options.cols;
                double out_y_point = options.out_h * y / options.rows;

                Character c = options.text.charAt(char_idx++ % options.text.length());

                double x_char = fm.charWidth(c) / 2.0;

                if (options.debug_draw)
                {
                    System.out.printf("%c @ I(%dx%d) -> O(%f-%fx%f+%f)%n", c, x, y, out_x_point, x_char - fontbox_x / 2, out_y_point, y_char + fontbox_y / 2);
                    g2d.setPaint(new Color(scaled_image.getRGB(x, y) & 0x40FFFFFF, true));
                    g2d.fillRect((int) out_x_point, (int) out_y_point , (int) fontbox_x, (int) fontbox_y);
                }

                int rgb = scaled_image.getRGB(x, y);
                if (options.font_size_mod)
                {
                    // who gives a shit about inverse gamma correction anyway
                    int r = rgb >> 16 & 0xFF;
                    int g = rgb >> 8 & 0xFF;
                    int b = rgb & 0xFF;
                    int lum = (r + r + r + b + g + g + g + g) >> 3;
                    double alpha = lum / 255.0;
                    double new_font_size = alpha * options.font_size_max+ (1 - alpha) * options.font_size_min;
                    new_font_size = new_font_size > 0 ? new_font_size : 0;
                    g2d.setFont(g2d.getFont().deriveFont((float) (options.font_size * new_font_size)));
                    fm = g2d.getFontMetrics(g2d.getFont());
                    y_char = (fm.getAscent() - fm.getDescent()) / 2.0;
                }

                g2d.setPaint(new Color(rgb)); // fucking `new`
                g2d.drawString(c.toString(), (float) (out_x_point - x_char + fontbox_x / 2.0), (float) (out_y_point + y_char + fontbox_y / 2.0));
            }
        }

        g2d.dispose();
        System.out.println("Done");
        return result;
    }

    public static String text_from_int(String dir, String dialogue_filter) throws TextProcessingException
    {
        File int_dir = new File(dir);
        if (!int_dir.isDirectory()) throw new TextProcessingException(".int directory invalid");

        StringBuilder sb = new StringBuilder();
        try
        {
            File[] files = int_dir.listFiles((dir1, name) -> name.toLowerCase().endsWith(".int"));
            for (File f : files)
            {
                BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(f), "UTF-16"));
                Stream<String> lines = br.lines();
                lines.forEach(line -> {
                    int eq_idx = line.indexOf('=');
                    if (eq_idx != -1 && line.substring(0, eq_idx).matches(dialogue_filter))
                    {
                        String sub_str = line.substring(eq_idx + 2, line.length() - 1);
                        if (!sub_str.startsWith("/!\\"))
                            sb.append(sub_str.replaceAll("\\|", " ")).append(" ");
                    }
                });
            }
        }
        catch (FileNotFoundException | UnsupportedEncodingException e) {throw new TextProcessingException(e);}

        System.out.println(String.format("Text length: %d", sb.length()));

        if (sb.length() == 0) throw new TextProcessingException("Text is empty");

        return sb.toString();
    }

    public static String text_from_file(String file) throws TextProcessingException
    {
        try
        {
            String result = new String(Files.readAllBytes(Paths.get(file)), StandardCharsets.UTF_8).replaceAll("\\R", "");
            if (result == null || result.length() == 0) throw new TextProcessingException("Text is empty");
            return result;
        }
        catch (IOException e) {throw new TextProcessingException("IOError", e);}
    }
}
